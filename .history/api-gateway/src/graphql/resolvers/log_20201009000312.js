const models = require('../models');


module.exports = {

    Query: {
        async findAllLogs() {
            return await models.Log.findAll({});
        },

        async findLog(_, { logId }, context) {
            return await models.Log.findByPk(logId);
        },
    },

    Log: {
        user(log) {
            return log.getUser();
        },

        events(log) {
            return log.getEvents();
        },
    },

}