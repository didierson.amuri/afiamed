import UserService from "../../services/User.service";
import InstitutionService from "../../services/Institution.service";
import HealthService from "../../services/Health.service";

const USER_TYPE_PATIENT = "";
const USER_TYPE_AGENT = "";

module.exports = {

    Mutation: {

        async createAgent(obj, { roleId, email, prenom, nom, password, phoneNumber }) {
            return await InstitutionService.createAgent({ roleId, email, prenom, nom, password, phoneNumber });
        },

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }

    },

};