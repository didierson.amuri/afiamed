import UserService from "../../services/User.service";

module.exports = {

    Mutation: {

        async verifyPhoneNumber(obj, { phoneNumber }) {
            return await UserService.verifyPhoneNumber({ phoneNumber });
        },

        async createUser(obj, { roleId, username, password, phoneNumber }) {
            return await UserService.createUser({ roleId, username, password, phoneNumber });
        },

        async createUserSession(obj, { username, password }, context) {
            context.res.cookie();
            return await UserService.createUserSession({ username, password });
        }

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }

    },

};