const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { AuthenticationError } = require('apollo-server-express');
const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');
const utils = require('../utils/utils');
const moment = require('moment');

const newDate = moment().format();
const defaultRole = '95a2d490-ce85-4a42-bb1d-a672c772fee3';
const defaultPassword = '123456';

module.exports = {

    Mutation: {

        async register(root, args, context) {
            const { roleId, afiaId, mutuelleId, prenom, nom, civilite, genre, dateNaissance, lieuNaissance, securiteSocial, situationFamiliale, profession,
                username, password, phoneNumber, email, adresse, createdBy, expireAt } = args.input;

            const userFound = await models.User.findOne({ where: { username: username } });
            console.log(userFound);
            if (!userFound) {
                const bcryptedPassword = await bcrypt.hash(password, 5);
                if (bcryptedPassword) {
                    return await models.User.create({
                        roleId, afiaId, mutuelleId, prenom, nom, civilite, genre, dateNaissance, lieuNaissance, securiteSocial, situationFamiliale, profession,
                        username, password: bcryptedPassword, phoneNumber, email, adresse, createdBy, expireAt
                    });
                } else {
                    return false;
                }
            } else {
                return false;
            }
        },

        async login(root, { input }, context) {
            const { username, password } = input;
            const user = await models.User.findOne({
                where: { username: username }
            });

            if (user && bcrypt.compareSync(password, user.password)) {
                user.update({ lastConnexion: newDate }, {
                    where: { id: user.id }
                });

                // appEvent = '6fb6fca5-c05a-47be-b1be-dbfbbfdc684d';
                // appUserAgent = parser(req.headers['user-agent']);
                // utils.addToLog(user.id, appEvent, appUserAgent, ipAddress);

                const jwtToken = jwtUtils.generateTokenForUser(user);
                return { ...user.toJSON(), jwtToken };
            }
            throw new AuthenticationError('Invalid credentials');
        },

    },

    Query: {
        async findAllUsers() {
            await models.User.findAll({
                attributes: ['id', 'civilite', 'nom', 'prenom', 'genre', 'phoneNumber', 'email', 'securiteSocial', 'adresse', 'username', 'lastConnexion', 'expireAt', 'status', 'createdAt', 'updatedAt'],
            }).then((res) => {
                console.log("RESS>>>", res);
                return res;
            }).catch((err) => {
                console.log(err);
            });
        },

        async findUser(_, { userId }, context) {
            return await models.User.findByPk(userId);
        },
    },

    User: {
        logs(user) {
            return user.getLogs();
        },

        role(user) {
            return user.getRole();
        },
    },

};