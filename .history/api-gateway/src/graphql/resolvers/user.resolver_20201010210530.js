import UserService from "../../services/User.service";
import InstitutionService from "../../services/Institution.service";
import HealthService from "../../services/Health.service";

const USER_TYPE_PATIENT = "";
const USER_TYPE_AGENT = "";

module.exports = {

    Mutation: {

        async verifyPhoneNumber(obj, { phoneNumber }) {
            return await UserService.verifyPhoneNumber({ phoneNumber });
        },

        async createUser(obj, { roleId, email, name, password, phoneNumber }) {
            const user = await UserService.createUser({ roleId, email, name, password, phoneNumber });
            if (user) {
                if (user.roleId === USER_TYPE_PATIENT) {
                    HealthService.createPatient({
                        userId: user.id,
                        
                    })
                }
            }
        },

        async createUserSession(obj, { username, password }, context) {
            const userSession = await UserService.createUserSession({ username, password });
            context.res.cookie("userSessionId", userSession.jwtToken, { httpOnly: true });
            return userSession;
        }

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }

    },

};