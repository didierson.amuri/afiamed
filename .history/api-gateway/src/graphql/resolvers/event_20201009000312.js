const models = require('../models');
const { AuthenticationError } = require('apollo-server-express');

module.exports = {

    Mutation: {

        async createEvent(_, { title, moduleId }, { user = null }) {
            if (!user) {
                throw new AuthenticationError('You must login to create a post');
              }
            return await models.Event.create({
                moduleId: moduleId,
                title,
            });
        },

    },

    Query: {
        async findAllEvents(root, args, context) {
            return await models.Event.findAll();
        },

        async findEvent(_, { eventId }, context) {
            return await models.Event.findByPk(eventId);
        },
    },

    Event: {
        module(event) {
            return event.getModule();
        },

        logs(event) {
            return event.getLogs();
        },
    },

}