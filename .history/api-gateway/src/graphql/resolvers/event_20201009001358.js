const { AuthenticationError } = require('apollo-server-express');

module.exports = {

    Mutation: {

    },

    Query: {

        async findAllEvents() {
            return await [
                {
                    id: "String",
                    title: "String",
                    status: "Int",
                    module: "Module",
                    logs: "[Log!]",
                    createdAt: "DateTime",
                    updatedAt: "DateTime",
                }
            ]
        }
       
    },

}