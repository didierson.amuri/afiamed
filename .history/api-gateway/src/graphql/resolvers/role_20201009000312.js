const models = require('../models');


module.exports = {

    Query: {
        async findAllRoles() {
            return await models.Role.findAll({});
        },

        async findRole(_, { id }, context) {
            return await models.Role.findByPk(id);
        },

    },

    Mutation: {

        async createRole(_, { title }, { user = null }) {
            const findRole = await models.Role.findOne({ where: { title: title } });
            if (!findRole) {
                return await models.Role.create({
                    title
                });
            }
            throw new Error('Role is already in use')
        },

        async updateRole(_, { id, title }, { user = null }) {
            const findRole = await models.Role.findOne({ where: { title: title } });
            if (!findRole) {
                const findRole2 = await models.Role.findOne({ where: { id: id } });
                if (findRole2) {
                    await findRole2.update({
                        id: id,
                        title: title
                    });
                    return true;
                }
                return false;
            }
            return false;
        },

        async deleteRole(_, {id}, { user = null}) {
            const findRole = await models.Role.findOne({ where: { id: id } });
            if (findRole) {
                findRole.destroy()
                return true;
            }
            return false;
        },

    },

}