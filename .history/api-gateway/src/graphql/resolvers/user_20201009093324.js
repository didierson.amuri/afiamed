import UserService  from "../../services/User.service";

module.exports = {

    Mutation: {

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }
        
    },

};