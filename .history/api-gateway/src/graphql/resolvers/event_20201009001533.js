const { AuthenticationError } = require('apollo-server-express');

module.exports = {

    Mutation: {

    },

    Query: {

        async findAllEvents() {
            return await [
                {
                    id: "String",
                    title: "String",
                    status: 1,
                    createdAt: "DateTime",
                    updatedAt: "DateTime",
                }
            ]
        }
       
    },

}