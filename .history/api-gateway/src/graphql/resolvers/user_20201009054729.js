import UserService  from "../../controllers/User.controller";

module.exports = {

    Mutation: {

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }
        
    },

};