import UserService from "../../services/User.service";

module.exports = {

    Mutation: {

        async verifyPhoneNumber(obj, { phoneNumber }) {
            return await UserService.verifyPhoneNumber({ phoneNumber });
        },

        async createUser(obj, { roleId, email, name, password, phoneNumber }) {
            return await UserService.createUser({ roleId, email, name, password, phoneNumber });
        },

        async createUserSession(obj, { username, password }, context) {
            const userSession = await UserService.createUserSession({ username, password });
            context.res.cookie("userSessionId", userSession.jwtToken, { httpOnly: true });
            return userSession;
        }

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }

    },

};