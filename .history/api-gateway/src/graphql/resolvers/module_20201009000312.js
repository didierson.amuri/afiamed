const models = require('../models');


module.exports = {

    Query: {
        async findAllModules() {
            return await models.Module.findAll({});
        },

        async findModule(_, { moduleId }, context) {
            return await models.Module.findByPk(moduleId);
        },
    },

    Mutation: {

        async createModule(_, { title, description }, { user = null }) {
            return await models.Module.create({
                title,
                description
            });
        },
       
    },

}