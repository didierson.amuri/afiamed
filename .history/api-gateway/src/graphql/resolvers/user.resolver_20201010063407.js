import UserService from "../../services/User.service";

module.exports = {

    Mutation: {

        async verifyPhoneNumber(obj, { phoneNumber }) {
            return await UserService.verifyPhoneNumber({ phoneNumber });
        },

        async createUser(obj, { roleId, username, password, phoneNumber }) {
            return await UserService.createUser({ roleId, username, password, phoneNumber });
        },

        async createUserSession(obj, { username, password }, context) {
            const userSession = await UserService.createUserSession({ username, password });
            context.res.cookie("userSessionId", userSession.id);
            return 
        }

    },

    Query: {

        async findAllUsers() {
            return await UserService.fetchAllUsers();
        }

    },

};