const { gql } = require('apollo-server-express');

module.exports = gql`

    type User {
        id: String!
        roleId: String
        username: String
        password: String
        sessionAt: String
        expireAt: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
        deletedAt: DateTime
    }

    extend type Query {
        findAllUsers: [User!]
        findUser(userId: String!): User
    }

    extend type Mutation {
         register(input: RegisterInput!): RegisterResponse
         login(input: LoginInput!): LoginResponse
    }

    input RegisterInput {
        roleId: String!
        afiaId: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String! 
        genre:String! 
        dateNaissance:String! 
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String 
        username:String! 
        password:String! 
        phoneNumber:String! 
        email:String
        adresse:String! 
        createdBy:String
        expireAt: DateTime!
    }

    type RegisterResponse {
        id: String
        afiaId: String
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        expireAt: DateTime!
    }

    input LoginInput {
        username: String!
        password: String!
    }

    type LoginResponse {
        id: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        jwtToken: String!
    }

`;