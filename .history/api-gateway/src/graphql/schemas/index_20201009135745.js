const { gql } = require('apollo-server-express');
const userType = require('./user');
const roleType = require('./role');
const moduleType = require('./module');
const eventType = require('./event.graphql');
const logType = require('./log');

const rootType = gql`

scalar DateTime

 type Query {
     root: String
 }
 type Mutation {
     root: String
 }
`;

module.exports = [rootType, userType, roleType, moduleType, eventType, logType];