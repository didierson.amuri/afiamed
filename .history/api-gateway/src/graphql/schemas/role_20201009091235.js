const { gql } = require('apollo-server-express');
import 

module.exports = gql`

    type Role {
        id: ID!
        title: String!
        status: Int!
        createdAt: DateTime!
        updatedAt: DateTime!
    }

    extend type Query {
        findAllRoles: [Role!]
        findRole(id: String!): Role
    }

    extend type Mutation {
        createRole(title: String!): Role!
        updateRole(id: String!, title: String!): Boolean!
        deleteRole(id: String!): Boolean!
    }

`;