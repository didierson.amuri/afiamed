const { gql } = require('apollo-server-express');

module.exports = gql`

    type User {
        id: String!
        roleId: String
        username: String
        password: String
        sessionAt: String
        expireAt: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
        deletedAt: DateTime
    }

    extend type Query {
        findAllUsers: [User!]
        findUser(userId: String!): User
    }

    extend type Mutation {
         register(input: RegisterInput!): RegisterResponse
         login(input: LoginInput!): LoginResponse
    }

    input RegisterInput {
        id: String!
        roleId: String
        username: String
        password: String
        sessionAt: String
        expireAt: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
        deletedAt: DateTime
    }

    type RegisterResponse {
        id: String!
        roleId: String
        username: String
        password: String
        sessionAt: String
        expireAt: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
        deletedAt: DateTime
    }

    input LoginInput {
        username: String!
        password: String!
    }

    type LoginResponse {
        id: String!
        roleId: String
        username: String
        password: String
        sessionAt: String
        expireAt: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
        deletedAt: DateTime
        jwtToken: String!
    }

`;