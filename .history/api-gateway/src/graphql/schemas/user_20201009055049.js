const { gql } = require('apollo-server-express');

module.exports = gql`

    type User {
        id: String!
        roleId: String
        etablissementId: String
        departementId: String
        matricule: String
        civilite: String
        nom: String
        prenom: String
        sexe: String
        phoneNumber: String
        email: String
        securiteSocial: String
        adresse: String
        username: String
        password: String
        lastConnexion: String
        expireAt: String
        avatar: String
        status: Int
        role: Role!
        logs: [Log!]
        createdAt: DateTime!
        updatedAt: DateTime!
    }

    extend type Query {
        findAllUsers: [User!]
        findUser(userId: String!): User
    }

    extend type Mutation {
         register(input: RegisterInput!): RegisterResponse
         login(input: LoginInput!): LoginResponse
    }

    input RegisterInput {
        roleId: String!
        afiaId: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String! 
        genre:String! 
        dateNaissance:String! 
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String 
        username:String! 
        password:String! 
        phoneNumber:String! 
        email:String
        adresse:String! 
        createdBy:String
        expireAt: DateTime!
    }

    type RegisterResponse {
        id: String
        afiaId: String
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        expireAt: DateTime!
    }

    input LoginInput {
        username: String!
        password: String!
    }

    type LoginResponse {
        id: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        jwtToken: String!
    }

`;