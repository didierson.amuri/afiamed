const { gql } = require('apollo-server-express');

module.exports = gql`

    type User {
        id: String!
        role: Role!
        logs: [Log!]
        mutuelleId: String
        afiaId: String!
        prenom: String!
        nom: String!
        civilite: String!
        genre: String!
        dateNaissance: String!
        lieuNaissance: String!
        securiteSocial: String
        situationFamiliale: String!
        profession: String
        groupeSanguin: String
        username: String!
        password: String!
        phoneNumber: String!
        email: String
        adresse: String
        avatar: String
        expireAt: String
        lastConnexion: String
        status: Int!
        createdBy: String
        createdAt: DateTime!
        updatedAt: DateTime!
    }

    extend type Query {
        findAllUsers: [User!]
        findUser(userId: String!): User
    }

    extend type Mutation {
         register(input: RegisterInput!): RegisterResponse
         login(input: LoginInput!): LoginResponse
    }

    input RegisterInput {
        roleId: String!
        afiaId: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String! 
        genre:String! 
        dateNaissance:String! 
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String 
        username:String! 
        password:String! 
        phoneNumber:String! 
        email:String
        adresse:String! 
        createdBy:String
        expireAt: DateTime!
    }

    type RegisterResponse {
        id: String
        afiaId: String
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        expireAt: DateTime!
    }

    input LoginInput {
        username: String!
        password: String!
    }

    type LoginResponse {
        id: String!
        mutuelleId: String
        prenom: String!
        nom: String!
        civilite: String!
        genre:String!
        dateNaissance:String!
        lieuNaissance:String!
        securiteSocial:String
        situationFamiliale:String!
        profession:String
        username:String!
        phoneNumber:String!
        email:String
        adresse:String!
        createdBy:String
        jwtToken: String!
    }

`;