import { gql } from "apollo-server";

const typeDefs = gql `

    scalar DateTime

    type Role {
        id: ID!
        agendaAt: String!
        agendaHeure: DateTime!
        status: Int!
    }

    type Query {
        findAllAgenda: [Role!]!
    }

`;

export default typeDefs;