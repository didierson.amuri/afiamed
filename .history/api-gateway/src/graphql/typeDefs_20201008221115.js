import { gql } from "apollo-server";

const typeDefs = gql `

    scalar DateTime

    type Agenda {
        id: ID!
        agendaAt: String!
        agendaHeure: DateTime!
        status: Int!
    }

`;