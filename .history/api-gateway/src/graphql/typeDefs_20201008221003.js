import { gql } from "apollo-server";

const typeDefs = gql `

    type Agenda {
        id: ID!
        agendaAt: String
        agendaHeure: DataTypes.TIME,
        status: Int
    }

`;