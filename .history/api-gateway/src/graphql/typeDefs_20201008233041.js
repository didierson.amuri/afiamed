import { gql } from "apollo-server";

const typeDefs = gql `

    scalar DateTime

    type Role {
        id: ID!
        title: String!
        status: Int!
    }

    type Query {
        roles: [Role!]!
    }

`;

export default typeDefs;