import got from "got";

const USERS_SERVICES_URI = "http://users-service:7100";

export default class UsersService {

    static async verifyPhoneNumber({ phoneNumber }) {
        const body = await got.post(`${USERS_SERVICES_URI}/sendSms`, {
            json: {phoneNumber}
        }).json();
        return body;
    }

    static async createUser({roleId, username, password, phoneNumber}) {
        const body = await got.post(`${USERS_SERVICES_URI}/users`, {
            json: {roleId, username, password, phoneNumber}
        }).json();
        return body;
    }

    static async createUserSession({ username, password }) {
        const body = await got.post(`${USERS_SERVICES_URI}/session`, {
            json: { username, password }
        }).json();
        return body;
    }

    static async createRole({ title }) {
        const body = await got.post(`${USERS_SERVICES_URI}/roles`, {
            json: { title }
        }).json();
        return body;
    }


    static async fetchAllUsers() {
        const body = await got.get(`${USERS_SERVICES_URI}/users`).json();
        return body;
    }

    static async fetchAllRoles() {
        const body = await got.get(`${USERS_SERVICES_URI}/users`).json();
        return body;
    }

    static async fetchAllModule() {
        const body = await got.get(`${USERS_SERVICES_URI}/modules`).json();
        return body;
    }

    static async fetchAllEvent() {
        const body = await got.get(`${USERS_SERVICES_URI}/events`).json();
        return body;
    }
}

