import got from "got";

const HEALTH_SERVICES_URI = "http://health-service:7102";

export default class HealthService {

    static async createPatient({ typeId, name, description, phoneNumber, email, website,
        adress1, adress2, city, state, zip, country }) {
        const body = await got.post(`${HEALTH_SERVICES_URI}/patient`, {
            json: {
                typeId, name, description, phoneNumber, email, website,
                adress1, adress2, city, state, zip, country
            }
        }).json();
        return body;
    }

}