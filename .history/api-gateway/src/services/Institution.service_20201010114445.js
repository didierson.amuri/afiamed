import got from "got";

const INSTITUTION_SERVICES_URI = "http://institution-service:7101";

export default class InstitutionService {

    static async createInstitution({roleId, username, password, phoneNumber}) {
        const body = await got.post(`${USERS_SERVICES_URI}/users`, {
            json: {roleId, username, password, phoneNumber}
        }).json();
        return body;
    }

}