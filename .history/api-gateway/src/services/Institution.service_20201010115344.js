import got from "got";

const INSTITUTION_SERVICES_URI = "http://institution-service:7101";

export default class InstitutionService {

    static async createInstitution({ typeId, name, description, phoneNumber, email, website,
        adress1, adress2, city, state, zip, country }) {
        const body = await got.post(`${INSTITUTION_SERVICES_URI}/institution`, {
            json: { roleId, username, password, phoneNumber }
        }).json();
        return body;
    }

}