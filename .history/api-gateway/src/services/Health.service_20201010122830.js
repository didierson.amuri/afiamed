import got from "got";

const HEALTH_SERVICES_URI = "http://health-service:7102";

export default class HealthService {

    static async createPatient({ userId, mutuelleId, prenom, nom, sexe, dateNaissance,
        lieuNaissance, securiteSocial, situationFamiliale, profession, groupeSanguin, phoneNumber,
        email, createdBy, adress1, adress2, city, state, zip, country }) {
        const body = await got.post(`${HEALTH_SERVICES_URI}/patient`, {
            json: {
                userId, mutuelleId, prenom, nom, sexe, dateNaissance,
                lieuNaissance, securiteSocial, situationFamiliale, profession, groupeSanguin, phoneNumber,
                email, createdBy, adress1, adress2, city, state, zip, country
            }
        }).json();
        return body;
    }

    static async fetchAllInstitutions() {
        const body = await got.get(`${HEALTH_SERVICES_URI}/institution`).json();
        return body;
    }

}