import got from "got";

const INSTITUTION_SERVICES_URI = "http://institution-service:7101";

export default class InstitutionService {

    static async createInstitution({ typeId, name, description, phoneNumber, email, website,
        adress1, adress2, city, state, zip, country }) {
        const body = await got.post(`${INSTITUTION_SERVICES_URI}/institution`, {
            json: {
                typeId, name, description, phoneNumber, email, website,
                adress1, adress2, city, state, zip, country
            }
        }).json();
        return body;
    }

    static async fetchAllInstitutions() {
        const body = await got.get(`${INSTITUTION_SERVICES_URI}/institution`).json();
        return body;
    }

    static async createDepartement({ institutionId, title, description }) {
        const body = await got.post(`${INSTITUTION_SERVICES_URI}/departement`, {
            json: { institutionId, title, description }
        }).json();
        return body;
    }

    static async fetchAllDepartement() {
        const body = await got.get(`${INSTITUTION_SERVICES_URI}/departement`).json();
        return body;
    }

    static async createFonction({ institutionId, title, description }) {
        const body = await got.post(`${INSTITUTION_SERVICES_URI}/fonction`, {
            json: { institutionId, title, description }
        }).json();
        return body;
    }

}