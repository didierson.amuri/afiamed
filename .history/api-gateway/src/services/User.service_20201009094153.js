import got from "got";

const USERS_SERVICES_URI = "http://users-service:7100";

export default class UsersService {

    static async createRole({title}) {
        const body = await got.post(`${USERS_SERVICES_URI}/roles`)
    }


    static async fetchAllUsers() {
        const body = await got.get(`${USERS_SERVICES_URI}/users`).json();
        return body;
    }

    static async fetchAllModule() {
        const body = await got.get(`${USERS_SERVICES_URI}/modules`).json();
        return body;
    }

    static async fetchAllEvent() {
        const body = await got.get(`${USERS_SERVICES_URI}/events`).json();
        return body;
    }
}

