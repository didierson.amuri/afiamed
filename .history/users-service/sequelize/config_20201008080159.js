    module.exports.development = {
        dialect: "mariadb",
        seederStorage: "sequelize",
        url: process.env.DB_URL
    }