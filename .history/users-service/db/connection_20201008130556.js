import { Sequelize } from "sequelize";

import accessEnv from "#root/helper/accessEnv";

const DB_URI = accessEnv("URI")