'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      eventId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'Events',
          key:'id'
        }
      }, 
      userId: {
        allowNull: true,
        type: Sequelize.UUID,
        references:{
          model:'Users',
          key:'id'
        }
      },
      patientId: {
        allowNull: true,
        type: Sequelize.UUID,
        references:{
          model:'Patients',
          key:'id'
        }
      },
      logAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      userAgent: {
        allowNull: true,
        type: Sequelize.JSON
      },
      ipAdress: {
        allowNull: true,
        type: Sequelize.STRING
      },
      geolocation: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Logs');
  }
};