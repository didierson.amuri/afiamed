'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('agendas', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      agendaAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      agendaHeure: {
        allowNull: false,
        type: Sequelize.TIME
      },
      userId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'users',
          key:'id'
        }
      },
      status: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('agendas');
  }
};