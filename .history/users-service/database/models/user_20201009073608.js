const { v4: uuid } = require('uuid');
const bcrypt = require('bcryptjs');

'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    sessionAt: DataTypes.DATE,
    expiresAt: DataTypes.DATE,
    status: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  User.beforeCreate(async (user) => {
    user.password = await user.generatePasswordHash();
    return user.id = uuid(); 
  });
  User.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Log, { foreignKey: 'userId', as: 'logs' });
    User.belongsTo(models.Role, { foreignKey: 'roleId', as: 'role' });
  };
  return User;
};