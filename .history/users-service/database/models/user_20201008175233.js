const { v4: uuid } = require('uuid');

'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    roleId: DataTypes.UUID,
    etablissementId: DataTypes.UUID,
    departementId: DataTypes.UUID,
    matricule: DataTypes.STRING,
    civilite: DataTypes.STRING,
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    securiteSocial: DataTypes.STRING,
    adresse: DataTypes.JSON,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    lastConnexion: DataTypes.DATE,
    expireAt: DataTypes.DATE,
    avatar: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  User.beforeCreate(async (user) => {
    user.password = await user.generatePasswordHash();
    return user.id = uuid(); 
  });
  User.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Log, { foreignKey: 'userId', as: 'logs' });
    User.belongsTo(models.Role, { foreignKey: 'roleId', as: 'role' });
  };
  return User;
};