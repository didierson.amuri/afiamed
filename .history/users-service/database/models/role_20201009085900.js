'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    title: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
    Role.hasMany(models.User, { foreignKey: 'roleId', as: 'users' });
  };
  return Role;
};