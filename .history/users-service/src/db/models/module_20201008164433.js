import { DataTypes, Model } from "sequelize";
import sequelize from "./../connection";

export class Module extends Model { }
Module.init({
  title: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true
  },
  description: {
    allowNull: true,
    type: Sequelize.TEXT
  },
  status: {
    allowNull: false,
    type: Sequelize.INTEGER
  },
  deletedAt: {
    allowNull: true,
    type: Sequelize.DATE
  }
}, {
  modelName: "modules",
  sequelize
});