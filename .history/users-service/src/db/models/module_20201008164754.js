import { DataTypes, Model } from "sequelize";
import sequelize from "../connection";

export class Module extends Model { }
Module.init(
  {
    title: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    description: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    status: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
  modelName: "modules",
  sequelize
});