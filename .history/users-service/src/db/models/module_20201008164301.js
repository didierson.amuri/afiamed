import { DataTypes, Model } from "sequelize";
import sequelize from "./../connection";

export class Module extends Model {}
Module.init( {
    title: {
      allowNull: false,
      type: Sequelize.STRING,
      unique: true
    },
    description: {
      allowNull: true,
      type: Sequelize.TEXT
    },
    status: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    deletedAt: {
      allowNull: true,
      type: Sequelize.DATE
    }
}, {
  
})

exports = (sequelize, DataTypes) => {
  const Module = sequelize.define('Module', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Module.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Module.associate = function(models) {
    // associations can be defined here
    Module.hasMany(models.Event, { foreignKey: 'moduleId', as: 'events' });
  };
  return Module;
};