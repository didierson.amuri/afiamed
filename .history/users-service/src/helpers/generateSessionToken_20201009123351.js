import bcrypt from "bcryptjs";
import jwt from 'jsonwebtoken';

const generateSessionToken = user => jwt.sign({
    id: user.id,
    username: user.username
})

bcrypt.hashSync(
    password, bcrypt.genSaltSync(12)
);

export default generateSessionToken;


// generateTokenForUser: function (userData) {
//     return jwt.sign({ 
//         id: userData.id, 
//         nom: userData.nom,
//         prenom: userData.prenom,
//         role: userData.TypePersonne.id
//     }, JWT_SIGN_SECRET, { expiresIn: '24h' }) //900000ms = 15min en ms
// }