import jwt from 'jsonwebtoken';

getUserId: function (authorization) {
    let userId = -1;
    let token = module.exports.parseAuthorization(authorization);
    if (token != null) {
        try {
            let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
            if (jwtToken != null)
                userId = jwtToken.id;
        } catch (err) {
        }
    }
    return userId;
}

import jwt from 'jsonwebtoken';

const mailTemplate = (authorization) => {
    let userId = -1;
    let token = module.exports.parseAuthorization(authorization);
    if (token != null) {
        try {
            let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
            if (jwtToken != null)
                userId = jwtToken.id;
        } catch (err) {
        }
    }
    return userId;
}

const getUserIdOnToken = user => jwt.sign({
    id: user.id, username: user.username,
    role: user.roleId
}, JWT_SIGN_SECRET, { expiresIn: '24h' });

export default getUserIdOnToken;