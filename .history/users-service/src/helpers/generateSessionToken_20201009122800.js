import bcrypt from "bcryptjs";

const generateSessionToken = password => bcrypt.hashSync(
    password, bcrypt.genSaltSync(12)
);

export default generateSessionToken;