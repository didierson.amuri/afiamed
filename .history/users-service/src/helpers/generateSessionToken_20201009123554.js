import jwt from 'jsonwebtoken';

const generateSessionToken = user => jwt.sign({
    id: user.id, username: user.username,
    role: user.roleId
}, JWT_SIGN_SECRET, { expiresIn: '24h' });

export default generateSessionToken;