getUserId: function (authorization) {
    let userId = -1;
    let token = module.exports.parseAuthorization(authorization);
    if (token != null) {
        try {
            let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
            if (jwtToken != null)
                userId = jwtToken.id;
        } catch (err) {
        }
    }
    return userId;
}