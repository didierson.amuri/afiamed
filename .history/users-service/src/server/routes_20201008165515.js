import Module from '#root/db/module';

const setupRoutes = app =>  {

    app.get('/users', async (req, res, next) => {
        const modules = await Module.findAll();
        return res.json(modules);
    });

};

export default setupRoutes;