import Nexmo from 'nexmo';
import models from '../../database/models';
import hashPassword from '#root/helpers/hashPassword';
import passwordCompareSync from '#root/helpers/passwordCompareSync';
import generateSessionToken from '#root/helpers/generateSessionToken';

const setupRoutes = app => {

    // Users

    const nexmo = new Nexmo({ apiKey: 'df5fdcc0', apiSecret: 'oLfLXCcdrAzLf8QU' });

    app.post('/sendSms', async (req, res, next) => {

        if (!req.body.phoneNumber) {
            return next(new Error("Missing mandatory information!"));
        }

        const smsCode = Math.floor(Math.random() * Math.floor(987654));
        const from = 'AFIAMED';
        const to = req.body.phoneNumber;
        const text = 'AfiaMed : Thank you for your registration, your verification code is ' + smsCode;

        const users = await models.User.findOne({ where: { phoneNumber: req.body.phoneNumber } });

        if (users) return next(new Error("This username already used"));

        try {
            nexmo.message.sendSms(from, to, text, (err, responseData) => {
                if (err) return err;
                if (responseData.messages[0]['status'] === "0") {
                    const smsResponse = {
                        phoneNumber: to,
                        smsCode: smsCode
                    };
                    return res.json(smsResponse);
                } else {
                    return next(new Error(`Message failed with error: ${responseData.messages[0]['error-text']}`))
                }
            })
        } catch (e) {
            return next(e);
        }
    })

    app.post('/session', async (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            return next(new Error("Missing mandatory information!"));
        }
        try {
            const userFound = await models.User.findOne({
                attributes: {},
                where: { username: req.body.username }
            });

            if (!userFound) return next(new Error("Incorect username or password !"));

            if (!passwordCompareSync(req.body.password, userFound.password)) {
                return next(new Error("Incorect username or password !"));
            }

            await userFound.update({ sessionAt: new Date() });

            const userRole = await models.Role.findOne({ where: { id: userFound.roleId } });

            const userSession = {
                id: userFound.id,
                roleId: userFound.roleId,
                username: userFound.username,
                role: userRole,
                status: userFound.status,
                expiresAt: userFound.expiresAt,
                createdAt: userFound.createdAt,
                updatedAt: userFound.updatedAt,
                jwtToken: generateSessionToken(userFound),
            }

            return res.json(userSession);

        } catch (e) {
            return next(e);
        }
    })

    app.post('/users', async (req, res, next) => {
        if (!req.body.username || !req.body.password ||
            !req.body.roleId || !req.body.roleId || !req.body.phoneNumber) {
            return next(new Error("Missing mandatory information!"));
        }

        const userExist = await models.User.findOne({
            where: { username: req.body.username }
        });

        if (userExist) return next(new Error('This username already used'));

        const phoneExist = await models.User.findOne({
            where: { phoneNumber: req.body.phoneNumber }
        });

        if (phoneExist) return next(new Error('This phone number already used'));

        let expireAt = new Date();
        expireAt.setDate(expireAt.getDate() + 365);

        try {
            const newUser = await models.User.create({
                roleId: req.body.roleId,
                username: req.body.username,
                password: hashPassword(req.body.password),
                phoneNumber: req.body.phoneNumber,
                expiresAt: expireAt
            })
            return res.json(newUser);
        } catch (e) {
            return next(e);
        }
    });

    // Roles
    app.post('/roles', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information"));
        }
        try {
            const exist = await models.Role.findOne({
                where: { title: req.body.title }
            });

            if (!exist) {
                const newRole = await models.Role.create({
                    title: req.body.title
                });
                return res.json(newRole);
            }
            return next(new Error("Exist information"));
        } catch (e) {
            return next(e);
        }
    });

    // Modules

    app.post('/modules', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information"));
        }
        try {
            const newModule = await models.Module.create({
                title: req.body.title,
                description: req.body.description
            });
            return res.json(newModule);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/users', async (req, res, next) => {
        try {
            const users = await models.User.findAll({
                include: [{ model: models.Role, required: false }],
            });
            return res.json(users);
        } catch (r) {
            return next(e);
        }
    });

    app.get('/events', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    });

    app.get('/modules', async (req, res, next) => {
        const modules = await models.Module.findAll();
        return res.json(modules);
    })

};

export default setupRoutes;