const models = require('../../database/models');

const setupRoutes = app =>  {

    app.get('/users', async (req, res, next) => {
        const modules = await models.Module.findAll();
        return res.json(modules);
    });

};

export default setupRoutes;