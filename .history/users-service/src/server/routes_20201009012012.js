const models = require('../../database/models');

const setupRoutes = app =>  {

    app.get('/users', async (req, res, next) => {
        const users = await models.User.findAll();
        return res.json(users);
    });

    app.get('/events', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    });

    app.get('/modules', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    })

};

export default setupRoutes;