import models from '../../database/models';
import hashPassword from '#root/helpers/hashPassword';
import passwordCompareSync from '#root/helpers/passwordCompareSync';
import generateSessionToken from '#root/helpers/generateSessionToken';

const setupRoutes = app => {

    // Users

    app.get('/sendSms', async (req, res, next) => {
        const users = await models.User.findOne({ where: { username: req.body.username }});

        if (users) return next(new Error("U"))
        return res.json(users);
    })

    app.post('/session', async (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            return next(new Error("Missing mandatory information!"));
        }
        try {
            const userFound = models.User.findOne({ attributes: {}, where: { username: username } })
            if (!userFound) return next(new Error("Incorect username or password !"));

            if (!passwordCompareSync(req.body.password, userFound.password)) {
                return next(new Error("Incorect username or password !"));
            }

            userFound.update({ sessionAt: new Date() });

            const userRole = await models.Role.findOne({ where: { id: userFound.roleId }});

            const userSession = {
                id: userFound.id,
                roleId: userFound.roleId,
                username: userFound.username,
                role: userRole,
                status: userFound.status,
                expiresAt: userFound.expiresAt,
                createdAt: userFound.createdAt,
                updatedAt: userFound.updatedAt,
                jwtToken: generateSessionToken(userFound),
            }

            return res.json(userSession);

        } catch (e) {
            return next(e);
        }
    })

    app.post('/users', async (req, res, next) => {
        if (!req.body.username || !req.body.password || !req.body.roleId) {
            return next(new Error("Missing mandatory information!"));
        }

        let expireAt = new Date();
        expireAt.setDate(expireAt.getDate() + 365);

        try {
            const newUser = await models.User.create({
                username: req.body.username,
                password: hashPassword(req.body.password),
                expiresAt: expireAt
            })
            return res.json(newUser);
        } catch (e) {
            return next(e);
        }
    });

    // Roles
    app.post('/roles', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information"));
        }
        try {
            const exist = await models.Role.findOne({
                where: { title: req.body.title }
            });

            if (!exist) {
                const newRole = await models.Role.create({
                    title: req.body.title
                });
                return res.json(newRole);
            }
            return next(new Error("Exist information"));
        } catch (e) {
            return next(e);
        }
    });

    // Modules

    app.post('/modules', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information"));
        }
        try {
            const newModule = await models.Module.create({
                title: req.body.title,
                description: req.body.description
            });
            return res.json(newModule);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/users', async (req, res, next) => {
        const users = await models.User.findAll();
        return res.json(users);
    });

    app.get('/events', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    });

    app.get('/modules', async (req, res, next) => {
        const modules = await models.Module.findAll();
        return res.json(modules);
    })

};

export default setupRoutes;