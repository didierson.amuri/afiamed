import bodyParser from "body-parser";
import cors from "cors";
import express from "express";

import accessEnv from "#root/helpers/accessEnv";

const PORT = accessEnv("PORT", 7200);

const app = express();

app.use(bodyParser);
app.use(cors({origin: (origin, cb) => cb(null, true), credentials: true}));

app.listen(PORT, "172.0.0.0", () => {
    console.log("Listen Users service on ")
})