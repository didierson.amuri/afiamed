import models from '../../database/models';
import hashPassword from '#root/helpers/hashPassword';

const setupRoutes = app => {

    // Modules

    app.post('/modules', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information"));
        }

        try {

        } catch (error) {

        }
    });

    // Users

    app.post('/users', async (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            return next(new Error("Invalid body!"));
        }
        
        let expireAt = new Date();
        expireAt.setDate(expireAt.getDate() + 365);

        try {

            const newUser = await models.User.create({
                username: req.body.username,
                password: hashPassword(req.body.password),
                expiresAt: expireAt
            })
        } catch (error) {

        }
    });

    app.get('/users', async (req, res, next) => {
        const users = await models.User.findAll();
        return res.json(users);
    });

    app.get('/events', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    });

    app.get('/modules', async (req, res, next) => {
        const modules = await models.Module.findAll();
        return res.json(modules);
    })

};

export default setupRoutes;