const models = require('#root/database/models');

const setupRoutes = app =>  {

    app.get('/users', async (req, res, next) => {
        const users = await models.User.findAll();
        return res.json(users);
    });

    app.get('/events', async (req, res, next) => {
        const events = await models.Event.findAll();
        return res.json(events);
    });

    app.get('/modules', async (req, res, next) => {
        const modules = await models.Module.findAll();
        return res.json(modules);
    })

};

export default setupRoutes;