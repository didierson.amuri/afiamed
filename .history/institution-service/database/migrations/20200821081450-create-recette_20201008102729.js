'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('recettes', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      recetteAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      consultationId: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      prestationId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'prestations',
          key:'id'
        }
      },
      patientId: {
        allowNull: false,
        type: Sequelize.UUID
      },
      designation: {
        allowNull: true,
        type: Sequelize.STRING
      },
      pu: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      qte: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      total: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      status: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      agentId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'agents',
          key:'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('recettes');
  }
};