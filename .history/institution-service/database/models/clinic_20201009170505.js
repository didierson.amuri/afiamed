const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Clinic = sequelize.define('Clinic', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    website: DataTypes.STRING,
    logo: DataTypes.STRING,
    image: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Clinic.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Clinic.associate = function(models) {
    // associations can be defined here
    Clinic.hasMany(models.Agent, { foreignKey: 'clinicId', as: 'agents' });
    Clinic.hasMany(models.Prestation, { foreignKey: 'clinicId', as: 'prestations' });
    Clinic.hasMany(models.Mutuelle, { foreignKey: 'clinicId', as: 'mutuelles' });
    Clinic.hasMany(models.Departement, { foreignKey: 'clinicId', as: 'departements' });

    Clinic.belongsTo(models.Type, { foreignKey: 'typeId', as: 'type' });
    Clinic.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return Clinic;
};