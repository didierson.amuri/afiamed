const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Type = sequelize.define('Type', {
    title: DataTypes.STRING
  }, {});
  Type.beforeCreate((res, _) => {
    return res.id = uuid();
  });
  Type.associate = function(models) {
    // associations can be defined here
    Type.hasMany(models.Institution, { foreignKey: 'institutionId', as: 'institutions' });
  };
  return Type;
};