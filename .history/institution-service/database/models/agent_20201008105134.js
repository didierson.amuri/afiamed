const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Agent = sequelize.define('Agent', {
    roleId: DataTypes.UUID,
    etablissementId: DataTypes.UUID,
    departementId: DataTypes.UUID,
    matricule: DataTypes.STRING,
    civilite: DataTypes.STRING,
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    securiteSocial: DataTypes.STRING,
    adresse: DataTypes.JSON,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    lastConnexion: DataTypes.DATE,
    expireAt: DataTypes.DATE,
    avatar: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  Agent.beforeCreate(async (user) => {
    user.password = await user.generatePasswordHash();
    return user.id = uuid(); 
  });
  Agent.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  Agent.associate = function(models) {
    // associations can be defined here
    Agent.hasMany(models.CompteRendu, { foreignKey: 'userId', as: 'compterendus' });
    User.hasMany(models.Mesure, { foreignKey: 'userId', as: 'mesures' });
    User.hasMany(models.Agenda, { foreignKey: 'userId', as: 'agendas' });
    User.hasMany(models.Consultation, { foreignKey: 'userId', as: 'consultations' });
    User.hasMany(models.File, { foreignKey: 'userId', as: 'courriers' });
    User.hasMany(models.Recette, { foreignKey: 'userId', as: 'recettes' });
    User.hasMany(models.Vaccin, { foreignKey: 'userId', as: 'vaccins' });
    User.hasMany(models.Analyse, { foreignKey: 'userId', as: 'analyses' });
    User.hasMany(models.Traitement, { foreignKey: 'userId', as: 'traitements' });
    User.hasMany(models.FicheObservation, { foreignKey: 'userId', as: 'ficheObservations' });
    User.hasMany(models.Prescription, { foreignKey: 'userId', as: 'prescriptions' });
    User.hasMany(models.Log, { foreignKey: 'userId', as: 'logs' });

    User.belongsTo(models.Role, { foreignKey: 'roleId', as: 'role' });
    User.belongsTo(models.Etablissement, { foreignKey: 'etablissementId', as: 'etablissement' });
    User.belongsTo(models.Departement, { foreignKey: 'departementId', as: 'departement' });
    User.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return User;
};