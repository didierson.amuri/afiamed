const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Institution = sequelize.define('Institution', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    website: DataTypes.STRING,
    logo: DataTypes.STRING,
    image: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Institution.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Institution.associate = function(models) {
    // associations can be defined here
    Institution.hasMany(models.Agent);
    Institution.hasMany(models.Prestation);
    Institution.hasMany(models.Mutuelle);
    Institution.hasMany(models.Departement);


    Institution.belongsTo(models.Type, { foreignKey: { allowNull: false } });
    Institution.belongsTo(models.Adresse, { foreignKey: { allowNull: false } });
  };
  return Institution;
};