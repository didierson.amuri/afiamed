const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Recette = sequelize.define('Recette', {
    recetteAt: DataTypes.DATE,
    designation: DataTypes.STRING,
    pu: DataTypes.INTEGER,
    qte: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {});
  Recette.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Recette.associate = function(models) {
    // associations can be defined here
    Recette.belongsTo(models.Consultation, { foreignKey: 'consultationId', as: 'consultation' });
    Recette.belongsTo(models.Prestation, { foreignKey: 'prestationId', as: 'prestation' });
    Recette.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' });
    Recette.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
  };
  return Recette;
};