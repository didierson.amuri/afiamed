const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Agent = sequelize.define('Agent', {
    userId: DataTypes.UUID,
    etablissementId: DataTypes.UUID,
    departementId: DataTypes.UUID,
    matricule: DataTypes.STRING,
    civilite: DataTypes.STRING,
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    securiteSocial: DataTypes.STRING,
    adresse: DataTypes.JSON,
    expireAt: DataTypes.DATE,
    avatar: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  Agent.beforeCreate(async (agent) => {
    agent.password = await agent.generatePasswordHash();
    return agent.id = uuid(); 
  });
  Agent.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  Agent.associate = function(models) {
    // associations can be defined here
    Agent.hasMany(models.Recette, { foreignKey: 'agentId', as: 'recettes' });
    Agent.hasMany(models.Prescription, { foreignKey: 'agentId', as: 'prescriptions' });

    Agent.belongsTo(models.Fonction, { foreignKey: 'fonctionId', as: 'fonction' });
    Agent.belongsTo(models.Institution, { foreignKey: 'institutionId', as: 'institution' });
    Agent.belongsTo(models.Departement, { foreignKey: 'departementId', as: 'departement' });
    Agent.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return Agent;
};