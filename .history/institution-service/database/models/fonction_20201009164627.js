const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Fonction = sequelize.define('Fonction', {
    title: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Fonction.beforeCreate((res, _) => {
    return res.id = uuid();
  });
  Fonction.associate = function(models) {
    // associations can be defined here
  };
  return Fonction;
};