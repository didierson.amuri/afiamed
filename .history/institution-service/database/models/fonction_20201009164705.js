const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Fonctions = sequelize.define('Fonctions', {
    title: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Fonctions.beforeCreate((res, _) => {
    return res.id = uuid();
  });
  Fonctions.associate = function(models) {
    // associations can be defined here
    Fonctions.hasMany(models.Agent, { foreignKey: 'fonctionId', as: 'agents' });
  };
  return Fonctions;
};