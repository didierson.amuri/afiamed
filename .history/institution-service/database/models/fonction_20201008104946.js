const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Agent = sequelize.define('Agent', {
    title: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {});
  Agent.beforeCreate((res, _) => {
    return res.id = uuid();
  });
  Agent.associate = function(models) {
    // associations can be defined here
    Agent.hasMany(models.Agent, { foreignKey: 'fonctionId', as: 'agents' });
  };
  return Agent;
};