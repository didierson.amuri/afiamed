const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Agent = sequelize.define('Agent', {
    roleId: DataTypes.UUID,
    etablissementId: DataTypes.UUID,
    departementId: DataTypes.UUID,
    matricule: DataTypes.STRING,
    civilite: DataTypes.STRING,
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    securiteSocial: DataTypes.STRING,
    adresse: DataTypes.JSON,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    lastConnexion: DataTypes.DATE,
    expireAt: DataTypes.DATE,
    avatar: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  Agent.beforeCreate(async (user) => {
    user.password = await user.generatePasswordHash();
    return user.id = uuid(); 
  });
  Agent.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  Agent.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Recette, { foreignKey: 'userId', as: 'recettes' });
    User.hasMany(models.Prescription, { foreignKey: 'userId', as: 'prescriptions' });

    Agent.belongsTo(models.Fonction, { foreignKey: 'fonctionId', as: 'fonction' });
    Agent.belongsTo(models.Institution, { foreignKey: 'institutionId', as: 'institution' });
    Agent.belongsTo(models.Departement, { foreignKey: 'departementId', as: 'departement' });
    Agent.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return Agent;
};