const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Etablissement = sequelize.define('Etablissement', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    website: DataTypes.STRING,
    logo: DataTypes.STRING,
    image: DataTypes.STRING,
    adresse: DataTypes.JSON,
    expiredAt: DataTypes.DATE,
    status: DataTypes.INTEGER
  }, {});
  Etablissement.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Etablissement.associate = function(models) {
    // associations can be defined here
    Etablissement.hasMany(models.User, { foreignKey: 'etablissementId', as: 'users' });
    Etablissement.hasMany(models.Prestation, { foreignKey: 'etablissementId', as: 'prestations' });
    Etablissement.hasMany(models.Mutuelle, { foreignKey: 'etablissementId', as: 'mutuelles' });

    Etablissement.belongsTo(models.TypeEtablissement, { foreignKey: 'typeEtablissementId', as: 'typeEtablissement' });
    Etablissement.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return Etablissement;
};