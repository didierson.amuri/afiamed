const models = require('../../database/models');

const setupRoutes = app => {

    app.post('/institution', async (req, res, next) => {
        if (!req.body.typeId || !req.body.name || !req.body.phoneNumber) {
            return next(new Error("Missing mandatory information!"));
        }

        const institutionFound = await models.Institution.findOne({
            where: { name: req.body.name, phoneNumber: req.body.phoneNumber }
        });

        if (institutionFound) return next(new Error('This institution name already used'));

        const addAdress = await models.Adresse.create({
            adress1: req.body.adress1, adress2: req.body.adress2, city: req.body.city,
            state: req.body.state, zip: req.body.zip, country: req.body.country
        });

        try {

        } catch (e) {
            return next(e);
        }
    });

    app.get('/institution', async (req, res, next) => {
        try {
            const hospital = await models.Institution.findAll();
            return res.json(hospital);
        } catch (e) {
            return next(e)
        }
    });

};

export default setupRoutes;