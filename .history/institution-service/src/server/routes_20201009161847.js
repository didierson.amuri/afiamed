const models = require('../../database/models');

const setupRoutes = app =>  {

    app.get('/institution', async (req, res, next) => {
        const hospital = await models.Institution.findAll();
        return res.json(hospital);
    });

};

export default setupRoutes;