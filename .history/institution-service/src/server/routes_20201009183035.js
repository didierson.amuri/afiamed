const models = require('../../database/models');

const setupRoutes = app => {

    // Type Institutions

    app.post('/type', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information!"));
        }

        const exist = await models.Type.findOne({ where: { title: req.body.title } });

        if (exist) return next(new Error('This institution type name already used'));

        try {
            const newType = await models.Type.create({ title: req.body.title });
            return res.json(newType);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/type', async (req, res, next) => {
        const type = await models.Type.findAll();
        return res.json(type);
    });

    // Institutions

    app.post('/institution', async (req, res, next) => {
        if (!req.body.typeId || !req.body.name || !req.body.phoneNumber) {
            return next(new Error("Missing mandatory information!"));
        }

        const institutionFound = await models.Institution.findOne({
            where: { name: req.body.name, phoneNumber: req.body.phoneNumber }
        });

        if (institutionFound) return next(new Error('This institution name already used'));

        let addAdress = null;
        try {
            addAdress = await models.Adresse.create({
                adress1: req.body.adress1, adress2: req.body.adress2, city: req.body.city,
                state: req.body.state, zip: req.body.zip, country: req.body.country
            });
        } catch (e) {
            return next(e);
        }

        try {

            const newInstitution = await models.Institution.create({
                typeId: req.body.typeId,
                adresseId: addAdress.id,
                name: req.body.name,
                description: req.body.description,
                phoneNumber: req.body.phoneNumber,
                email: req.body.email,
                website: req.body.website
            });

            return res.json(newInstitution);

        } catch (e) {
            return next(e);
        }
    });

    app.get('/institution', async (req, res, next) => {
        try {
            const hospital = await models.Institution.findAll({
                include: [
                    { model: models.Adresse, as: "adresse" },
                    { model: models.Type, as: "type" }
                ],
                order: [['name', 'asc']]
            });
            return res.json(hospital);
        } catch (e) {
            return next(e)
        }
    });

    app.get('/institution-by-id', async (req, res, next) => {
        const hospital = await models.Institution.findOne({
            where: { id: req.body.id },
            include: [
                { model: models.Adresse, as: "adresse" },
                { model: models.Type, as: "type" }
            ]
        });;
        return res.json(hospital);
    });

    app.get('/institution-by-type', async (req, res, next) => {
        const hospital = await models.Institution.findAll({
            where: { typeId: req.body.typeId },
            include: [
                { model: models.Adresse, as: "adresse" },
                { model: models.Type, as: "type" }
            ]
        });;
        return res.json(hospital);
    })

    // Departements
    app.post('/departement', async (req, res, next) => {
        if (!req.body.title) {
            return next(new Error("Missing mandatory information!"));
        }
    })
};

export default setupRoutes;