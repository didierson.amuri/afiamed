const models = require('../../database/models');

const setupRoutes = app => {

    app.post('/institution', async (req, res, next) => {
        if (!req.body.typeId || !req.body.adresseId ||
            !req.body.name || !req.body.phoneNumber) {
            return next(new Error("Missing mandatory information!"));
        }

        const institutionFound = await models.Institution.findOne({where: { name: req.body.name }});

        if (institutionFound) return next(new Error('This institution name already used'));

        try {

        } catch (e) {
            return next(e);
        }
    });

    app.get('/institution', async (req, res, next) => {
        try {
            const hospital = await models.Institution.findAll();
            return res.json(hospital);
        } catch (e) {
            return next(e)
        }
    });

};

export default setupRoutes;