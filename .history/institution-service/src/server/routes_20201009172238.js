const models = require('../../database/models');

const setupRoutes = app => {

    app.post('/institution', async (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            return next(new Error("Missing mandatory information!"));
        }
        try {
            
        } catch (e) {
            return next(e);
        }
    });

    app.get('/institution', async (req, res, next) => {
        try {
            const hospital = await models.Institution.findAll();
            return res.json(hospital);
        } catch (e) {
            return next(e)
        }
    });

};

export default setupRoutes;