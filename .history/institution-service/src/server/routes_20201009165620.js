const models = require('../../database/models');

const setupRoutes = app => {

    app.get('/institution', async (req, res, next) => {
        try {
            const hospital = await models.Prestation.findAll();
            return res.json(hospital);
        } catch (e) {
            return next(e)
        }
    });

};

export default setupRoutes;