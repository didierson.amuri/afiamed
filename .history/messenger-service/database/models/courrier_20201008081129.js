const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Courrier = sequelize.define('Courrier', {
    courrierAt: DataTypes.DATE,
    from: DataTypes.UUID,
    to: DataTypes.TEXT,
    modele: DataTypes.STRING,
    objet: DataTypes.STRING,
    message: DataTypes.JSON,
    status: DataTypes.INTEGER
  }, {});
  Courrier.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Courrier.associate = function(models) {
    // associations can be defined here
  };
  return Courrier;
};