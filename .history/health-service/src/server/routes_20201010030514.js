const Sequelize = require("sequelize");
const models = require('../../database/models');
import generateAfiaId from '#root/helpers/generateAfiaID';

const Op = Sequelize.Op;

const setupRoutes = app => {

    // Patients

    app.post('/patient', async (req, res, next) => {
        if (!req.body.nom || !req.body.prenom ||
            req.body.sexe || req.body.dateNaissance || req.body.lieuNaissance ||
            req.body.phoneNumber || !req.body.adress1 || req.body.city || req.body.state || req.body.country) {
            return next(new Error("Missing mandatory information!"));
        }

        const exist = await models.Agent.findOne({
            where: {
                prenom: req.body.prenom,
                nom: req.body.nom,
                phoneNumber: req.body.phoneNumber
            }
        });
        if (exist) return next(new Error('This patient already exist'));

        let addAdress = null;
        try {
            addAdress = await models.Adresse.create({
                adress1: req.body.adress1, adress2: req.body.adress2, city: req.body.city,
                state: req.body.state, zip: req.body.zip, country: req.body.country
            });
        } catch (e) {
            return next(e);
        }

        try {
            const newPatient = await models.Patient.create({
                afiaId: generateAfiaId(1),
                userId: req.body.userId,
                mutuelleId: req.body.mutuelleId,
                adresseId: addAdress.id,
                prenom: req.body.prenom,
                nom: req.body.nom,
                sexe: req.body.sexe,
                dateNaissance: req.body.dateNaissance,
                lieuNaissance: req.body.lieuNaissance,
                securiteSocial: req.body.securiteSocial,
                situationFamiliale: req.body.situationFamiliale,
                profession: req.body.profession,
                groupeSanguin: req.body.groupeSanguin,
                phoneNumber: req.body.phoneNumber,
                email: req.body.email,
                createdBy: req.body.createdBy
            });
            return res.json(newPatient);
        } catch (e) {
            return next(e);
        }

    })

    app.get('/patient', async (req, res, next) => {
        try {
            const patients = await models.Patient.findAll({
                include: [
                    { model: models.Adresse, as: "adresse" }
                ],
                order: [['prenom', 'asc']]
            });
            return res.json(patients);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-id', async (req, res, next) => {
        try {
            const patient = await models.Patient.findOne({
                where: { id: req.body.id },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-afiaid', async (req, res, next) => {
        try {
            const patient = await models.Patient.findOne({
                where: { afiaId: req.body.afiaId },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-userid', async (req, res, next) => {
        try {
            const patient = await models.Patient.findOne({
                where: { userId: req.body.userId },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-agent-create', async (req, res, next) => {
        try {
            const patient = await models.Patient.findAll({
                where: { createdBy: req.body.agentId },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ],
                order: [['prenom', 'asc']]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-search', async (req, res, next) => {
        try {
            const patient = await models.Patient.findAll({
                where: {
                    [Op.or]: [{ nom: { [Op.substring]: `${req.body.search}` } },
                    { prenom: { [Op.substring]: `${req.body.search}` } },
                    { afiaId: { [Op.substring]: `${req.body.search}` } },
                    { securiteSocial: { [Op.substring]: `${req.body.search}` } },
                    { phoneNumber: { [Op.substring]: `${req.body.search}` } }]
                },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ],
                order: [['prenom', 'asc']]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    // Mesures

    app.post('/mesure', async (req, res, next) => {
        if (!req.body.key || req.body.value || !req.body.patientId ||
            !req.body.patientId) {
            return next(new Error("Missing mandatory information!"));
        }

        const exist = await models.Mutuelle.findOne({
            where: { title: req.body.title, institutionId: req.body.institutionId }
        });
        if (exist) return next(new Error('This mutuelle name already exist'));
    });

};

export default setupRoutes;