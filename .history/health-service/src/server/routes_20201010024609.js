const models = require('../../database/models');
import generateAfiaId from '#root/helpers/generateAfiaID';

const setupRoutes = app => {

    // Patients

    app.post('/patient', async (req, res, next) => {
        if (!req.body.nom || !req.body.prenom ||
            req.body.sexe || req.body.dateNaissance || req.body.lieuNaissance ||
            req.body.phoneNumber || !req.body.adress1 || req.body.city || req.body.state || req.body.country) {
            return next(new Error("Missing mandatory information!"));
        }

        const exist = await models.Agent.findOne({
            where: {
                prenom: req.body.prenom,
                nom: req.body.nom,
                phoneNumber: req.body.phoneNumber
            }
        });
        if (exist) return next(new Error('This patient already exist'));

        let addAdress = null;
        try {
            addAdress = await models.Adresse.create({
                adress1: req.body.adress1, adress2: req.body.adress2, city: req.body.city,
                state: req.body.state, zip: req.body.zip, country: req.body.country
            });
        } catch (e) {
            return next(e);
        }

        try {
            const newPatient = await models.Patient.create({
                afiaId: generateAfiaId(1),
                userId: req.body.userId,
                mutuelleId: req.body.mutuelleId,
                adresseId: addAdress.id,
                prenom: req.body.prenom,
                nom: req.body.nom,
                sexe: req.body.sexe,
                dateNaissance: req.body.dateNaissance,
                lieuNaissance: req.body.lieuNaissance,
                securiteSocial: req.body.securiteSocial,
                situationFamiliale: req.body.situationFamiliale,
                profession: req.body.profession,
                groupeSanguin: req.body.groupeSanguin,
                phoneNumber: req.body.phoneNumber,
                email: req.body.email,
                createdBy: req.body.createdBy
            });
            return res.json(newPatient);
        } catch (e) {
            return next(e);
        }

    })

    app.get('/patient', async (req, res, next) => {
        try {
            const patients = await models.Patient.findAll();
            return res.json(patients);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-id', async (req, res, next) => {
        try {
            const patient = await models.Patient.findOne({
                where: { id: req.body.id },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

    app.get('/patient-by-agent-create', async (req, res, next) => {
        try {
            const patient = await models.Patient.findOne({
                where: { createdBy: req.body.id },
                include: [
                    { model: models.Adresse, as: "adresse" }
                ]
            });
            return res.json(patient);
        } catch (e) {
            return next(e);
        }
    });

};

export default setupRoutes;