const models = require('../../database/models');

const setupRoutes = app => {

    // Patients

    app.post('/patient', async (req, res, next) => {
        if (!req.body.nom || !req.body.prenom ||
            req.body.sexe || req.body.dateNaissance || req.body.lieuNaissance || 
            req.body.phoneNumber || !req.body.adress1 || req.body.city || req.body.state || req.body.country) {
            return next(new Error("Missing mandatory information!"));
        }
    })

    app.get('/patient', async (req, res, next) => {
        try {
            const patients = await models.Patient.findAll();
            return res.json(patients);
        } catch (e) {
            return next(e);
        }
    });

};

export default setupRoutes;