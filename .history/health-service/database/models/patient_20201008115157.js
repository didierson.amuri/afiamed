const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Patient = sequelize.define('Patient', {
    prenom: DataTypes.STRING,
    nom: DataTypes.STRING,
    sexe: DataTypes.STRING,
    dateNaissance: DataTypes.DATE,
    lieuNaissance: DataTypes.STRING,
    securiteSocial: DataTypes.STRING,
    situationFamiliale: DataTypes.STRING,
    profession: DataTypes.STRING,
    groupeSanguin: DataTypes.STRING,
    afiaId: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    telDomicile: DataTypes.STRING,
    telPro: DataTypes.STRING,
    email: DataTypes.STRING,
    avatar: DataTypes.STRING,
    expireAt: DataTypes.DATE,
    status: DataTypes.INTEGER,
    createdBy: DataTypes.UUID
  }, {
    defaultScope: {
      rawAttributes: { exclude: ['password'] },
    },
  });
  Patient.beforeCreate(async (patient) => {
    patient.password = await patient.generatePasswordHash();
    return patient.id = uuid(); 
  });
  Patient.prototype.generatePasswordHash = function () {
    if (this.password) {
      return bcrypt.hash(this.password, 10);
    }
  };
  Patient.associate = function(models) {
    // associations can be defined here
    Patient.hasMany(models.Recette, { foreignKey: 'patientId', as: 'recettes' });
    Patient.hasMany(models.Mesure, { foreignKey: 'patientId', as: 'mesures' });
    Patient.hasMany(models.Antecedent, { foreignKey: 'patientId', as: 'antecedents' });
    Patient.hasMany(models.Agenda, { foreignKey: 'patientId', as: 'agendas' });
    Patient.hasMany(models.Vaccin, { foreignKey: 'patientId', as: 'vaccins' });
    Patient.hasMany(models.File, { foreignKey: 'patientId', as: 'files' });
    Patient.hasMany(models.Consultation, { foreignKey: 'patientId', as: 'consultations' });
    Patient.hasMany(models.Contact, { foreignKey: 'patientId', as: 'contacts' });
    Patient.hasMany(models.Prescription, { foreignKey: 'patientId', as: 'prescriptions' });
    Patient.hasMany(models.Traitement, { foreignKey: 'patientId', as: 'traitements' });
    Patient.hasMany(models.FicheObservation, { foreignKey: 'patientId', as: 'ficheObservations' });
    Patient.hasMany(models.Analyse, { foreignKey: 'patientId', as: 'analyses' });
    Patient.hasMany(models.Log, { foreignKey: 'patientId', as: 'logs' });

    Patient.belongsTo(models.Mutuelle, { foreignKey: 'mutuelleId', as: 'mutuelle' });
    Patient.belongsTo(models.Adresse, { foreignKey: 'adresseId', as: 'adresse' });
  };
  return Patient;
};