const { v4: uuid } = require('uuid');
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Traitement = sequelize.define('Traitement', {
    debutAt: DataTypes.DATE,
    finAt: DataTypes.DATE,
    intitule: DataTypes.STRING,
    commentaire: DataTypes.TEXT,
    status: DataTypes.INTEGER,
  }, {});
  Traitement.beforeCreate((res, _) => {
    return res.id = uuid(); 
  });
  Traitement.associate = function(models) {
    // associations can be defined here
    Traitement.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' });
    Traitement.belongsTo(models.Medicament, { foreignKey: 'medicamentId', as: 'medicament' });
    Traitement.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
  };
  return Traitement;
};