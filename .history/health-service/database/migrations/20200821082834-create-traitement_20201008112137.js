'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('traitements', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: uuid()
      },
      code: {
        allowNull: false,
        type: Sequelize.STRING
      },
      patientId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'Patients',
          key:'id'
        }
      },
      medicamentId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'Medicaments',
          key:'id'
        }
      },
      debutAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      finAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      intitule: {
        allowNull: false,
        type: Sequelize.STRING
      },
      commentaire: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      status: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      userId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'Users',
          key:'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Traitements');
  }
};