'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('contacts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      patientId: {
        allowNull: false,
        type: Sequelize.UUID,
        references:{
          model:'patients',
          key:'id'
        }
      },
      medecinTraitant: {
        allowNull: false,
        type: Sequelize.UUID
      },
      medecinReferent: {
        allowNull: true,
        type: Sequelize.UUID
      },
      correspodant: {
        allowNull: true,
        type: Sequelize.UUID
      },
      lienFamiliaux: {
        allowNull: true,
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('contacts');
  }
};